<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportUser;
use App\Http\Requests\UserImportRequest;
use  App\Events\ImportFailEvent;
use App\Jobs\ImportUserJob;
use Maatwebsite\Excel\HeadingRowImport;
class UserImportController extends Controller
{
    public function import(UserImportRequest $request)
    {
        try{
            $file = $request->file('file');
            $file = $request->file('file')->store('temp');
            $path = storage_path('app') . '/' . $file;
            dispatch(new ImportUserJob($path));
            return response()->json(['status' => 'success', 'message' => 'Operations successfully queued and will be imported soon.'],201);
        } catch (Exception $e) {
            $failures = $e->failures();
            return response()->json(['status' => 'error', 'message' => 'Operations failed.'],400);
        }
    }
}
