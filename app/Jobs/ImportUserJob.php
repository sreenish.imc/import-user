<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportUser;
use App\Events\ImportFailEvent;
use Maatwebsite\Excel\HeadingRowImport;
use App\Traits\ImportUserTrait;
class ImportUserJob implements ShouldQueue
{
    use ImportUserTrait;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $file;
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            Excel::import(new ImportUser(), $this->file);
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $failer_array = [];
            foreach ($failures as $failure) {
                $failer_array[] = $failure->errors()[0].$failure->row();
            }
            #send mail
            event(new ImportFailEvent($failer_array));
        }
    }
}
