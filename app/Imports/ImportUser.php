<?php

namespace App\Imports;

use App\Models\ImportedUser;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportUser implements ToCollection,WithValidation,WithHeadingRow, WithChunkReading
{
    public function rules(): array
    {
        return [
            'user_code' => 'required|regex:/^[a-bA-B0-9 ]+$/',
            'user_name' => 'required',
            'user_address' => 'required',
        ];
    }

    public function customValidationMessages()
    {
        return [
            'user_code.required'    => 'User Code is missing at row ',
            'user_code.regex'       => 'User Code contains symbols at row ',
            'user_name.required'    => 'User Name is missing at row ',
            'user_address.required' => 'User Address is missing at row ',
        ];
    }


    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            ImportedUser::create([
                'user_code' => $row['user_code'],
                'user_name' => $row['user_name'],
                'user_address' => $row['user_address'],
            ]);
        }
    }

    public function batchSize(): int
    {
        return 500;
    }

    public function chunkSize(): int
    {
        return 500;
    }

}
