<?php
namespace App\Traits;
use DateTime;
use DateTimeZone;
trait ImportUserTrait
{
    function validateHeader($valid_headers,$imported_headers)
    {   
        $valid_headers = array_map('strtolower', $valid_headers);
        $imported_headers = array_map('strtolower', $imported_headers);
        if(count(array_diff($valid_headers, $imported_headers)) > 0) 
        {
            foreach($imported_headers as $field) {
                echo $field . ' is not valid header';
                $errors[$field] = "Missing or wrong header: ".$field. " (valid: " . implode(", ", $valid_headers) .")";
            }
            throw ValidationException::withMessages($errors);
        }
    }
}