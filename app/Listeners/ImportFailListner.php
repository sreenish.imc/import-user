<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ImportFailEvent;
use App\Mail\ImportFailMail;
use Illuminate\Support\Facades\Mail;
class ImportFailListner
{

    public function handle(ImportFailEvent $event)
    {
        $data = array('errors'=>$event->errors);
        $emails = ['akshara@protracked.in','rahul@protracked.in'];
        try {
            Mail::to($emails)->send(new ImportFailMail($data));
            return response()->json(['status' => 'success', 'message' => 'Mail sent successfully']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if( count(Mail::failures()) > 0 ) {
            return response()->json('Something Went Wrong in Mail',422);
        }
    }
}
