<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="_token" content="{{ csrf_token() }}" />
    <title>Import User</title>
</head>
<body>
    <div>
        <h1>Import Users</h1>
        <form method="post" id="upload-csv-form" enctype="multipart/form-data">
            <input type="file" id="file">
            <button type="submit" class="btn" id="upload_csv">Upload</button>
        </form>
        <div id="error_div"></div>
        <p id="success"></p>
      </div>
</body>
</html>

<script src="http://code.jquery.com/jquery-3.3.1.min.js">
</script>
<script>
    $(document).ready(function(){
        $('#upload-csv-form').submit(function(e) {
            e.preventDefault();
            var file_data = $('#file').prop('files')[0];
            let formData = new FormData(this);
            formData.append('file', file_data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('api/user/import') }}",
                method: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function(result){
                    console.log(result.message)
                    $('#file').val('');
                    $('#success').html(result.message);
                    setTimeout(function(){
                            $('#success').html('');
                        }, 3000);
                },
                error:function (response) {
                    response.responseJSON.errors.file.forEach(function(error){
                        $('#error_div').append('<p class="error">'+error+'</p>');
                        setTimeout(function(){
                            $('#error_div').html('');
                        }, 3000);
                    });
                }
            });
        });
    });
</script>


<style>
html {
  height: 100%;
}

body {
  background: #000000;
  color: #FFFFFF;
}

div {
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  font-family: 'Roboto Mono', monospace;
}
.btn{
    margin-top: 25px;
}
.error{
    color: red;
}
.success{
    color: green;
}
</style>


